﻿/***************************************************************************

Copyright (c) Microsoft Corporation. All rights reserved.
THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Globalization;

namespace SCAFFOLD.DOMEProject
{
    public interface IViewModel
    {
        string Name { get; set; }
        string Version { get; set; }
        List<DomeClient.Entities.Project.Resource> Resources { get; set; }

        event EventHandler ViewModelChanged;
        void DoIdle();
        void Close();

        void OnSelectChanged(object p);
    }
}