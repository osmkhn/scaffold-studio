# README #

Scaffold Studio is a Visual Studio 2015 based IDE for editing DOME models. In its current state it is developed as a set of plugins that can install into an existing VS instance and provide the ability to modify DOME objects.

### How do I get set up? ###

* Clone the repo
* Open the Solution in VS 2015
* Under "References" make sure you're pointing to the latest build of DomeClient.Entities DLL
* Build and Run to launch an experimental instance of VS 2015
* Load on of the files inside the "Samples" folder
* Edit and Save the file to test the plugin


### Current status ###
The current release only has support for loading DOME Project (.dpj) files. Only a few basic fields are editable right now. Also, you can only edit existing files as the File > New functionality for creating new DOME projects doesn't exist yet. The goal of this release was to get the basic framework for the plugin up and running. Other fields as well as support for the rest of DOME objects like Models, Interfaces etc. will be added going forward based on this framework.